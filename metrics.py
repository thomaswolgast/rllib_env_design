from typing import Dict, Optional, Tuple, Union
from ray.rllib.env import BaseEnv
from ray.rllib.evaluation.episode_v2 import EpisodeV2
from ray.rllib.policy import Policy
from ray.rllib.policy.sample_batch import SampleBatch
from ray.rllib.evaluation import RolloutWorker, Episode
from ray.rllib.algorithms.callbacks import DefaultCallbacks
from ray.rllib.utils.typing import PolicyID


class OPFMetrics(DefaultCallbacks):
    def on_episode_start(self, *, worker: RolloutWorker, base_env: BaseEnv, policies: Dict[str, Policy], episode: Episode, env_index: Optional[int] = None, **kwargs):
        episode.custom_metrics = {}

    def on_episode_step(self, *, worker: "RolloutWorker", base_env: BaseEnv, policies: Optional[Dict[PolicyID, Policy]] = None, episode: Union[Episode, EpisodeV2], env_index: Optional[int] = None, **kwargs,):
        # Implementation not required.
        pass

    def on_episode_end(self, *, worker: RolloutWorker, base_env: BaseEnv, policies: Dict[str, Policy], episode: Union[Episode, EpisodeV2], env_index: Optional[int] = None, **kwargs):
        episode.custom_metrics['valids'] = float(worker.env.info['valids'].all())
        episode.custom_metrics['obj'] = worker.env.info['objectives'].sum()
        episode.custom_metrics['violations'] = worker.env.info['violations'].sum()
        if worker.env.eval:
            opt_obj = worker.env.baseline_reward()
            episode.custom_metrics['regret'] = opt_obj - episode.custom_metrics['obj']
            episode.custom_metrics['ape'] = episode.custom_metrics['regret'] / abs(opt_obj) * 100
            if episode.custom_metrics['valids']:
                episode.custom_metrics['valid_ape'] = episode.custom_metrics['ape']
                episode.custom_metrics['valid_regret'] = episode.custom_metrics['regret']

    def on_sample_end(self, worker: RolloutWorker, samples: SampleBatch, **kwargs):
        pass

    def on_train_result(self, *, algorithm, result: dict, **kwargs):
        # Implementation not required.
        pass

    def on_postprocess_trajectory(self, *, worker: RolloutWorker, episode: Episode, agent_id: str, policy_id: str, policies: Dict[str, Policy], postprocessed_batch: SampleBatch,
                                  original_batches: Dict[str, Tuple[Policy, SampleBatch]], **kwargs):
        pass

    # def on_evaluate_end(
    #         self,
    #         *,
    #         algorithm: "Algorithm",
    #         evaluation_metrics: dict,
    #         **kwargs,
    #     ) -> None:
    #         """Runs when the evaluation is done.

    #         Runs at the end of Algorithm.evaluate().

    #         Args:
    #             algorithm: Reference to the algorithm instance.
    #             evaluation_metrics: Results dict to be returned from algorithm.evaluate().
    #                 You can mutate this object to add additional metrics.
    #             kwargs: Forward compatibility placeholder.
    #         """
    #         print(evaluation_metrics['evaluation']['custom_metrics'])