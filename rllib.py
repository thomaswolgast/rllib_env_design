import argparse
import datetime
import os
import time
import warnings

from pprint import pprint

from ray.rllib.algorithms.sac import SACConfig
from ray.rllib.algorithms.ppo import PPOConfig
from ray.rllib.algorithms.td3 import TD3Config  
from ray.rllib.algorithms.ddpg import DDPGConfig  
import ray
import torch

from metrics import OPFMetrics
from data_handling import metrics_to_csv


def main():
    os.environ["TUNE_DISABLE_AUTO_CALLBACK_LOGGERS"] = "1"

    parser = argparse.ArgumentParser()
    parser.add_argument('--workers', type=int, default=1)
    parser.add_argument('--algorithm', type=str, default='SAC')
    parser.add_argument('--hyperparams', type=str, default='{}')
    parser.add_argument('--env-config', type=str, default='{}')
    parser.add_argument('--environment', type=str, default='mlopf.envs.thesis_envs.QMarketEnv')
    parser.add_argument('--num-runs', type=int, default=1)
    parser.add_argument('--steps-train', type=int, default=10000)
    parser.add_argument('--eval-interval', type=int, default=1000)
    parser.add_argument('--eval-duration', type=int, default=100)
    parser.add_argument('--path', type=str, default='results/tmp/')
    args = parser.parse_args()

    warnings.filterwarnings("ignore", category=ResourceWarning)
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    ray.init(address="auto", log_to_driver=False, include_dashboard=False, dashboard_host="0.0.0.0")

    for n in range(args.num_runs):
        train_run(args)
 
    ray.shutdown()


def train_run(args):
    # Use smaller network for QMarket and bigger net for EcoDispatch
    fcnet_hiddens = 3 * [256] if "QMarket" in args.environment else 4 * [256]

    if args.algorithm == "SAC":
        config = SACConfig()
        hyperparams = {
            'q_model_config': {"fcnet_hiddens": fcnet_hiddens},
            'policy_model_config': {"fcnet_hiddens": fcnet_hiddens},
            'train_batch_size': 1024,  # TODO Default 256 here?!
            '_enable_learner_api': False}
    elif args.algorithm == "PPO":
        config = PPOConfig()
        hyperparams = {
            'sgd_minibatch_size': 1024,  # TODO: Default 128 here?!
            'model': {"fcnet_hiddens": fcnet_hiddens},
            '_enable_learner_api': False}    
    elif args.algorithm == "TD3" or args.algorithm == "DDPG":
        if args.algorithm == "TD3":
            config = TD3Config()
        else:
            config = DDPGConfig()
        hyperparams = {
            'actor_hiddens': fcnet_hiddens,
            'critic_hiddens': fcnet_hiddens,
            'train_batch_size': 1024, # Default 256
            '_enable_learner_api': False}   

    hyperparams.update(eval(args.hyperparams)) 
    config = config.training(**hyperparams)

    config = config.exploration(explore=True, 
                                exploration_config={"type": "StochasticSampling"})

    num_gpus = 1 if torch.cuda.is_available() else 0
    print('Num GPUs: ', num_gpus, '-----------------')
    config = config.resources(num_gpus=num_gpus, 
                            num_cpus_per_worker=1, 
                            num_cpus_per_learner_worker=1)

    config = config.rollouts(batch_mode="complete_episodes", 
                            num_envs_per_worker=1, 
                            num_rollout_workers=args.workers, 
                            rollout_fragment_length=1, 
                            observation_filter="MeanStdFilter",
                            preprocessor_pref=None)

    config = config.framework(framework="torch")

    env_config = {"eval": False}
    env_config.update(eval(args.env_config))
    config = config.environment(env=args.environment,
                                env_config=env_config,
                                disable_env_checking=True,
                                normalize_actions=True)

    config = config.rl_module(_enable_rl_module_api=False)

    config = config.reporting(min_sample_timesteps_per_iteration=0, 
                            min_time_s_per_iteration=0, 
                            metrics_num_episodes_for_smoothing=1)

    config = config.evaluation(evaluation_interval=args.eval_interval,  # Remove
                            evaluation_duration=args.eval_duration,
                            evaluation_config={"explore": False, 
                                                "env_config": {"eval": True}})

    config = config.callbacks(OPFMetrics)

    algo = config.build()

    name = (datetime.datetime.now().strftime("%Y%m%d-%H%M%S") 
            + '_' + args.algorithm 
            + '_' + args.environment.split('.')[-1] + '/')
    path = os.path.join(args.path, name)
    os.makedirs(path, exist_ok=True)

    # Store hyperparams to JSON in path
    with open(os.path.join(path, 'hyperparams.json'), 'w') as f:
        f.write(str(hyperparams))
    # And the environment config as well
    with open(os.path.join(path, 'env_config.json'), 'w') as f:
        f.write(str(env_config))

    idx = 0
    eval_interval = args.eval_interval // args.workers
    start_time = time.time()
    while True:
        step = idx * args.workers
        train_metrics = algo.train()

        # pprint(train_metrics)

        if step >= args.steps_train:
            evaluate(algo, path, step, train_metrics, start_time)
            break
        if idx % eval_interval == 0 and step > 0:
            evaluate(algo, path, step, train_metrics, start_time)

        idx += 1

    algo.save()

def evaluate(algo, path, step, train_metrics, start_time):
    print(f'Evaluate at episode {step}')
    metrics = algo.evaluate()
    print('Valids: ', metrics['evaluation']['custom_metrics']['valids_mean'])
    try:
        print('Valid MAPE: ', metrics['evaluation']['custom_metrics']['valid_ape_mean'])
    except:
        print('Valid MAPE: No valid solutions at all...')
    print('--------------')

    metrics['evaluation']['custom_metrics']['train_step'] = train_metrics['num_agent_steps_trained']
    metrics['evaluation']['custom_metrics']['step'] = step
    metrics['evaluation']['custom_metrics']['time'] = time.time() - start_time

    algo.save(checkpoint_dir=path)
    metrics_to_csv(metrics['evaluation']['custom_metrics'], 
                   os.path.join(path, 'metrics.csv'))

if __name__ == '__main__':
    main()

